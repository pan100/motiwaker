import React, { Component } from 'react';
import SpeechModuleAbstract from "./SpeechModuleAbstract";
import {
    Text,
    View,
    TextInput
} from 'react-native';

import Button from 'react-native-button';
import I18n from 'react-native-i18n';
import Speech from 'react-native-speech';

import { AsyncStorage } from 'react-native';
import { globals } from "./Globals";

export default class WeatherMVAModule extends SpeechModuleAbstract {
    constructor() {
        super();
        this.state = {
            sentence: I18n.t('weather') + ": ",
        }
        this.prepare();
    }

    prepare() {
        let geoUrl = globals.wundergroundRoot + "geolookup/q/";
        navigator.geolocation.getCurrentPosition(
      (position) => {
        let posString = position.coords.longitude + "," + position.coords.latitude;
        //mock posString
        posString = "60.3911,5.3247";
        geoUrl += posString + ".json";
        console.log(geoUrl);
        fetch(geoUrl)
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson);
        if(responseJson.response.error != undefined) {
            if(responseJson.response.error.type === "querynotfound") {
                this.state.sentence += I18n.t('noWeather');    
            } 
        }
        else {
            this.state.sentence += I18n.t("place") + ": ";
            this.state.sentence += responseJson.location.city + ". ";
            //TODO GET LANGUAGE CODE
            let langString = "EN";
            if(I18n.currentLocale().substr(0,2) === "nb") {
                langString ="NO";
            }
            let weatherUrl = globals.wundergroundRoot + "hourly/lang:" + langString + "/" + responseJson.location.l + ".json"
            fetch(weatherUrl)
            .then((responseW) => responseW.json())
            .then((responseWJson) => {
                for(let i=0; i<=11; i+=4) {
                    let weather = responseWJson.hourly_forecast[i];
                    this.state.sentence += I18n.t("time")+ ": " + weather.FCTTIME.pretty + ", ";
                    this.state.sentence += weather.temp.metric + " " + I18n.t("degCel") + ". " + weather.condition + ". ";
                    this.state.sentence += I18n.t("windStrength") + ": " + weather.wspd.metric + " " + I18n.t("mps") + " "
                     + weather.wdir.degrees + " " + I18n.t("degrees") + ". ";
                }
                console.log(this.state.sentence,"sentence");
            })
            .catch((error) => {
                console.error(error);
            });
        }
      })
      .catch((error) => {
        console.error(error);
      });

      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
    }

    getSentence() {
        return this.state.sentence;
    }

    speak() {
        Speech.speak({
            text: this.getSentence(),
            voice: I18n.locale
        });
    }

    getSettingsComponent() {
        return <View>
            <Button
                style={{ fontSize: 20, color: 'green' }}
                styleDisabled={{ color: 'red' }}
                onPress={() => this.speak()}>
                {I18n.t('test')}
            </Button>
            <Button
                style={{ fontSize: 20, color: 'green' }}
                styleDisabled={{ color: 'red' }}
                onPress={() => this.stop()}>
                {I18n.t('stop')}
            </Button>
        </View>
    }
}