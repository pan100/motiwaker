import React, { Component } from 'react';
import SpeechModuleAbstract from "./SpeechModuleAbstract";
import {
    Text,
    View,
    TextInput
} from 'react-native';

import Button from 'react-native-button';
import I18n from 'react-native-i18n';
import Speech from 'react-native-speech';

import { AsyncStorage } from 'react-native';
import { globals } from "./Globals";
import RNCalendarEvents from 'react-native-calendar-events';

export default class CalendarMWAModule extends SpeechModuleAbstract {
    constructor() {
        super();
        this.state = {
            sentence: "call getSentence for sentence."
        }
        this.prepare();
    }

    authorize() {
        RNCalendarEvents.authorizeEventStore();
    }

    prepare() {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        RNCalendarEvents.authorizeEventStore()
            .then(status => {
                RNCalendarEvents.fetchAllEvents(new Date().toISOString(), tomorrow.toISOString())
                    .then(events => {
                        if(events.length == 0) {
                            this.state.sentence = I18n.t('calNoneNext24');
                        }
                        else {
                            this.state.sentence = I18n.t('calUpcoming24');
                            for(event of events) {
                                let startTime = new Date(event.occurrenceDate);
                                startTime.setDate(startTime.getHours() + globals.timeOffset);
                                let endTime = new Date(event.endDate);
                                endTime.setDate(endTime.getHours() + globals.timeOffset);
                                let startMinutes = startTime.getMinutes();
                                let endMinutes = endTime.getMinutes();
                                if(startMinutes < 10) {
                                    startMinutes = "0" + startMinutes;
                                }
                                if(endMinutes < 10) {
                                    endMinutes = "0" + endMinutes;
                                }
                                let startTimeString = startTime.getHours() + " " + startMinutes;
                                let endTimeString = endTime.getHours() + " " + endMinutes;
                                this.state.sentence += I18n.t('calAtTime')
                                                    + startTimeString
                                                    + I18n.t('calTo')
                                                    + endTimeString
                                                    + ": "
                                                    + event.title
                                                    + I18n.t('calLocation')
                                                    + event.location
                            }
                        }

                    })
                    .catch(error => {
                        this.state.sentence = I18n.t('failedAccessCal');
                    });
            })
            .catch(error => {
                this.state.sentence = I18n.t('failedAccessCal');
            });
    }

    getSentence() {
        return this.state.sentence;
    }

    speak() {
        Speech.speak({
            text: this.getSentence(),
            voice: I18n.locale
        });
    }

    getSettingsComponent() {
        return <View>
            <Button
                style={{ fontSize: 20, color: 'green' }}
                styleDisabled={{ color: 'red' }}
                onPress={() => this.speak()}>
                {I18n.t('test')}
            </Button>
            <Button
                style={{ fontSize: 20, color: 'green' }}
                styleDisabled={{ color: 'red' }}
                onPress={() => this.stop()}>
                {I18n.t('stop')}
            </Button>
        </View>
    }
}