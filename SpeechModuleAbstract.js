import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';

import Speech from 'react-native-speech';
import Button from 'react-native-button';
import I18n from 'react-native-i18n';

import {globals} from "./Globals";

export default class SpeechModuleAbstract {
    constructor() {
        //if (new.target === SpeechModuleAbstract) {
        //    throw new TypeError("Cannot construct instances of SpeechModuleAbstract directly");
        //}
  }
      
    stop() {
      Speech.stop();
    }
    speak() {
        
        Speech.speak({
            text: this.getSentence(),
            voice: I18n.locale
});
    }
    getSentence() {
        return "All subclasses must return a sentence for speech"
    }
    prepare() {
        //called before the alarm goes off - if async needed for sentence, call this
    }
    authorize() {
        //called when app starts - needed if you need to ask for permissions.
    }
    getSettingsComponent() {
        return <View>
            <Text>Settings dummy</Text>
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.speak()}>
                test
            </Button>
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.stop()}>
                stop
            </Button>            
        </View>
    }
}