import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    StyleSheet,
    Switch
} from 'react-native';

import I18n from 'react-native-i18n';
import AlarmManager from "./AlarmManager";

export default class WeekdayTogglesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            monday: true,
            tuesday: true,
            wednesday: true,
            thursday: true,
            friday: true,
            saturday: false,
            sunday: false
        }
        this.alarmManager = AlarmManager.instance();
        this.alarmManager.loadInitialState().then(function () {
            this.state.monday = this.alarmManager.settings.repeatDay[1];
            this.state.tuesday = this.alarmManager.settings.repeatDay[2];
            this.state.wednesday = this.alarmManager.settings.repeatDay[3];
            this.state.thursday = this.alarmManager.settings.repeatDay[4];
            this.state.friday = this.alarmManager.settings.repeatDay[5];
            this.state.saturday = this.alarmManager.settings.repeatDay[6];
            this.state.sunday = this.alarmManager.settings.repeatDay[7];
            this.forceUpdate();
        }.bind(this));
    }

    setMonday(value) {
        this.alarmManager.setRepeatDay(1);
        this.setState({ monday: value })
    }
     setTuesday(value) {
        this.alarmManager.setRepeatDay(2);
        this.setState({ tuesday: value })
    }   
    setWednesday(value) {
        this.alarmManager.setRepeatDay(3);
        this.setState({ wednesday: value })
    }
    setThursday(value) {
        this.alarmManager.setRepeatDay(4);
        this.setState({ thursday: value })
    }
    setFriday(value) {
        this.alarmManager.setRepeatDay(5);
        this.setState({ friday: value })
    }
    setSaturday(value) {
        this.alarmManager.setRepeatDay(6);
        this.setState({ saturday: value })
    }
    setSunday(value) {
        this.alarmManager.setRepeatDay(7);
        this.setState({ sunday: value })
    }

    render() {
        return (
            <View>
                <View style={styles.flexRow}>
                    <Text>{I18n.t("monday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setMonday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.monday} />
                    <Text>{I18n.t("tuesday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setTuesday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.tuesday} />
                    <Text>{I18n.t("wednesday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setWednesday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.wednesday} />
                </View>
                <View style={styles.flexRow}>
                    <Text>{I18n.t("thursday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setThursday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.thursday} />
                    <Text>{I18n.t("friday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setFriday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.friday} />
                </View>
                <View style={styles.flexRow}>
                    <Text>{I18n.t("saturday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setSaturday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.saturday} />
                    <Text>{I18n.t("sunday")}</Text>
                    <Switch
                        onValueChange={(value) => this.setSunday(value)}
                        style={{ marginLeft: 10 }}
                        value={this.state.sunday} />
                </View>
            </View>)
    }
}

var styles = StyleSheet.create({
    flexRow: {
        flexDirection: 'row'
    }
});