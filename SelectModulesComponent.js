import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View
} from 'react-native';
import Button from 'react-native-button';
import CheckBox from 'react-native-checkbox';
import I18n from 'react-native-i18n';

import ModuleRegister from "./ModuleRegister";

export default class SelectModulesComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
        this.moduleRegister = ModuleRegister.instance();
}

  render() {
      let componentsCheckBoxArray = [];
      for(let module of this.props.modules) {
          let element = 
          <CheckBox
            label={I18n.t("mr_" + module.id + "_title" )}
            key={module.id}
            checked={module.enabled}
            onChange={(checked) => this.toggleCheckedModule(module)}
/>
          componentsCheckBoxArray.push(element);
      }
    return (
      <View>
        {componentsCheckBoxArray}  
      </View>
    );
  }
  toggleCheckedModule(module) {
      this.moduleRegister.setActive(module);
      this.setState({modules : this.moduleRegister.getModules()});
      this.props.update();
  }
}