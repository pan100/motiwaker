/*
* Modules:
* Custom greeting sentence
* Time and date
* Calendar briefing
* RSS
* Web radio
* Weather
*/
import { AsyncStorage } from 'react-native';
import { globals } from "./Globals";
import I18n from 'react-native-i18n';

import CustomGreetingSentenceMWAModule from "./CustomGreetingSentenceMWAModule";
import TimeAndDateMWAModule from "./TimeAndDateMWAModule";
import CalendarMWAModule from "./CalendarMWAModule";
import WeatherMWAModule from "./WeatherMWAModule";

let _singleton = Symbol();

class ModuleRegister {

    modules = [];
    module_classes = {
        GREET: CustomGreetingSentenceMWAModule,
        TIME: TimeAndDateMWAModule,
        CAL : CalendarMWAModule,
        WEATHR: WeatherMWAModule
    };
    mwaModules = {};

    constructor(singletonToken) {
        if (_singleton !== singletonToken) {
            throw new Error('Cannot instantiate directly.');
        }
        //load state from storage
        this.loadInitialState().done();
    }

    static instance() {
        if (!this[_singleton])
            this[_singleton] = new ModuleRegister(_singleton);

        return this[_singleton]
    }

    loadInitialState = async () => {
        try {

            var value = await AsyncStorage.getItem(globals.storage + ":modules");
            if (value !== null) {
                this.modules = JSON.parse(value);
                console.log('Recovered selection from disk: ' + value);
            } else {
                this.modules = [
                    {
                        title: {
                            en: "Custom greeting sentence",
                            nb: "Egenkomponert setning"
                        },
                        id: "GREET",
                        enabled: true,
                        priority: 1
                    },
                    {
                        title: {
                            en: "Time and date",
                            nb: "Tid og dato"
                        },
                        id: "TIME",
                        enabled: true,
                        priority: 2
                    },
                    {
                        title: {
                            en: "Calendar briefing",
                            nb: "Kalenderbrifing"
                        },
                        id: "CAL",
                        enabled: true,
                                  priority: 3
                    },  
                    // {
                    //     title: {
                    //         en: "RSS",
                    //         nb: "RSS"
                    //     },
                    //     id: "RSS",
                    //     enabled: false,
                    //     priority: 4
                    // },
                    // {
                    //     title: {
                    //         en: "Web radio",
                    //         nb: "Nettradio"
                    //     },
                    //     id: "RADIO",
                    //     enabled: false,
                    //     priority: 6
                    // },
                     {
                         title: {
                             en: "Weather",
                             nb: "Været"
                         },
                         id: "WEATHR",
                         enabled: true,
                         priority: 5
                      }
                ];
                console.log('Initialized with no selection on disk.');
            }
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
        this._addTranslations();
        this._loadMwaModules();
    };

    _onValueChange = async () => {
        try {
            await AsyncStorage.setItem(globals.storage + ":modules", JSON.stringify(this.modules));
            console.log('Saved selection to disk: ' + this.modules);
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    authorizeMWAModules() {
        for (let module in this.mwaModules) {
            if (this.mwaModules.hasOwnProperty(module)) {
                this.mwaModules[module].authorize();
            }
        }
    } 

    prepareMWAModules() {
        for (let module in this.mwaModules) {
            if (this.mwaModules.hasOwnProperty(module)) {
                this.mwaModules[module].prepare();
            }
        }
    } 

    _loadMwaModules() {
        for (module of this.modules) {
            if (module.id in this.module_classes) {
                this.mwaModules[module.id] = new this.module_classes[module.id]();
            }
        }
    }
    getModules() {
        this.modules.sort((function (a, b) {
            if (a.priority > b.priority) {
                return 1;
            }
            if (a.priority < b.priority) {
                return -1;
            }
            // a must be equal to b
            return 0;
        }));
        return this.modules;
    }

    getMwaModule(id) {
        return this.mwaModules[id];
    }

    setActive(module) {
        for (let iterModule of this.modules) {
            if (iterModule.id === module.id) {
                iterModule.enabled = !iterModule.enabled;
            }
        }
        this._onValueChange().done();
    }

    deleteSavedData = async () => {
        try {
            await AsyncStorage.removeItem(globals.storage + ":modules");
            console.log('Selection removed from disk.');
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    _getTranslations(languageCode) {
        translationStore = {};
        for (let module of this.modules) {
            if (languageCode in module.title) {
                var translation = {}
                translation["mr_" + module.id + "_title"] = module.title[languageCode];
                translationStore = Object.assign(translationStore, translation);
            }
        }
        return translationStore;
    }
    _addTranslations() {
        I18n.translations.en = Object.assign(I18n.translations.en, this._getTranslations("en"));
        I18n.translations.nb = Object.assign(I18n.translations.nb, this._getTranslations("nb"));
    }
}

export default ModuleRegister;