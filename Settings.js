import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Switch,
  Picker
} from 'react-native';
import Button from 'react-native-button';
import Speech from 'react-native-speech';
import CheckBox from 'react-native-checkbox';
import I18n from 'react-native-i18n';

import StartPage from "./StartPage";
import SelectModulesComponent from "./SelectModulesComponent";
import ModuleSettingsComponent from "./ModuleSettingsComponent";
import TimePickerComponent from "./TimePickerComponent";
import WeekdayTogglesComponent from "./WeekdayTogglesComponent";

import ModuleRegister from "./ModuleRegister";
import AlarmManager from "./AlarmManager";

export default class Alarm extends Component {
  constructor(props) {
    super(props);
    this.moduleRegister = ModuleRegister.instance();
    this.state = {
      modules: this.moduleRegister.getModules(),
      alarmOn: false,
      repeatToggle: true,
      snoozeTime: 10
    }
    this.alarmManager = AlarmManager.instance();
    this.alarmManager.loadInitialState().then(function() {
      this.state.alarmOn = this.alarmManager.settings.alarmOn;
      this.state.repeatToggle = this.alarmManager.settings.repeat;
      this.forceUpdate();
    }.bind(this));
  }

  setAlarmOn(value) {
    this.alarmManager.changeValue("alarmOn", value);
    this.setState({alarmOn: value})
  }
  setRepeatToggle(value) {
    this.alarmManager.changeValue("repeat", value);
    this.setState({repeatToggle: value})
  }

  setSnoozeTime(value) {
    this.alarmManager.changeValue("snoozeTime", value);
    this.setState({snoozeTime: value})    
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.welcome}>
            {I18n.t('titleSettings')}
          </Text>
          <View style={styles.flexRow}>
          <Text style={styles.instructions}>
            {I18n.t('alarmOnOff')}
          </Text>
          <Switch
          onValueChange={(value) => this.setAlarmOn(value)}
          style={{marginLeft: 10}}
          value={this.state.alarmOn} />
          </View>
          <Text style={styles.instructions}>
            {I18n.t('alarmTime')}
          </Text>
          <TimePickerComponent />
          <Text style={styles.instructions}>
            {I18n.t('repeat')}
          </Text>
          <Switch
          onValueChange={(value) => this.setRepeatToggle(value)}
          style={{marginLeft: 10}}
          value={this.state.repeatToggle} />          
          {this._hideShowWeekdayTogglesComponent()}
          <Text style={styles.instructions}>
            {I18n.t('timeSnooze')}
          </Text>
             <Picker
                style={styles.picker}
                selectedValue={this.state.snoozeTime}
                onValueChange={(value) => this.setSnoozeTime(value)}>
                <Picker.Item label={5 + I18n.t('minutes')} value={5} />
                <Picker.Item label={10 + I18n.t('minutes')} value={10} />
                <Picker.Item label={15 + I18n.t('minutes')} value={15} />
            </Picker>         
          <Text style={styles.instructions}>
            {I18n.t('modules')}
          </Text>
          <SelectModulesComponent modules={this.state.modules} update={this.update.bind(this)} />
          <Text style={styles.instructions}>
            {I18n.t('moduleSettings')}
          </Text>
          <ModuleSettingsComponent modules={this.state.modules} />

          <Button
            style={{ fontSize: 20, color: 'green' }}
            styleDisabled={{ color: 'red' }}
            onPress={() => this._goBack()}>
            {I18n.t('goBack')}
          </Button>
        </View>
      </ScrollView>
    );
  }

  _hideShowWeekdayTogglesComponent() {
    if(this.state.repeatToggle) {
      return(<WeekdayTogglesComponent/>);
    }
  }

  update() {
    this.forceUpdate();
  }

  _goBack() {
    this.props.navigator.push({
      component: StartPage,
      title: 'Start Page',
    })
  }
}

const styles = StyleSheet.create({
  flexRow: {
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: "red",
  },
  instructions: {
    fontSize: 18,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  smaller: {
    fontSize: 18,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
      picker: {
        width: 100,
        flex: 2,
    }
});