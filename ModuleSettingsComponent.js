import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Button from 'react-native-button';
import CheckBox from 'react-native-checkbox';
import I18n from 'react-native-i18n';

import ModuleRegister from "./ModuleRegister";

export default class ModuleSettingsComponent extends Component {
    constructor(props){
        super(props);
        this.moduleRegister = ModuleRegister.instance();
}

  render() {
      let componentsArray = [];
      let onlyEnabledModules = 
        this.props.modules.filter(function(module) {
            return module.enabled;
        });
      for(let module of onlyEnabledModules) {
          let settingsComponent = null;
          let mwa = this.moduleRegister.getMwaModule(module.id);
          if(mwa) {
              settingsComponent = mwa.getSettingsComponent();
          }

          let moduleSetting = 
          <View key={module.id} style={styles.box}>
          <Text style={styles.subinstruction}>{I18n.t("mr_" + module.id + "_title" )}</Text>
          {settingsComponent}
          </View>;
          componentsArray.push(moduleSetting);
      }
    return (
      <View>
        {componentsArray} 
      </View>
    );
  }
  toggleCheckedModule(module) {
      this.state.moduleRegister.setActive(module);
      this.setState({modules : this.state.moduleRegister.getModules()});
  }
}

const styles = StyleSheet.create({
    subinstruction: {
        fontWeight: "bold"
    },
  box: {
    backgroundColor: '#FFFFFF',
    borderColor: '#000033',
    borderWidth: 1,
  },
});