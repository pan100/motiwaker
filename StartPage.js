import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';
import Button from 'react-native-button';
import Speech from 'react-native-speech';
import I18n from 'react-native-i18n';

import Recommend from './Recommend';
import Alarm from './Alarm';
import Settings from './Settings';
import ModuleRegister from "./ModuleRegister";
import AlarmManager from "./AlarmManager";
import LocalNotification from '@remobile/react-native-local-notifications';

export default class StartPage extends Component {
  constructor(props) {
    super(props);
    this.moduleRegister = ModuleRegister.instance();
    this.moduleRegister.loadInitialState().done(function () {

    });
    this.moduleRegister.authorizeMWAModules();
    this.alarmManager = AlarmManager.instance();
    LocalNotification.registerPermission(function (granted) { });

  }
  static get defaultProps() {
    return {
      title: 'Speech Alarm - Start'
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {I18n.t('title')}
        </Text>
        <Text style={styles.instructions}>
          {I18n.t('subtitle')}
        </Text>

        <Button
          style={{ fontSize: 20, color: 'green' }}
          styleDisabled={{ color: 'red' }}
          onPress={() => this._goSettings()}>
          {I18n.t('settings')}
        </Button>
        <Button
          style={{ fontSize: 20, color: 'green' }}
          styleDisabled={{ color: 'red' }}
          onPress={() => this.goTestAlarm()}>
          {I18n.t('testAlarm')}
        </Button>
      </View>
    );
  }
  _deleteSavedData() {
    this.moduleRegister.deleteSavedData();
    this.alarmManager.deleteSavedData();
  }
  _goSettings() {
    this.props.navigator.push({
      component: Settings,
      title: 'Settings',
    })
  }
  goTestAlarm() {
    this.props.navigator.push({
      component: Alarm,
      title: 'Alarm',
    });
    this.moduleRegister.prepareMWAModules();
    setTimeout(function () {
      Alarm.playAlarm();
    }, 1000);
  }
  _thirtySecTest() {
    setTimeout(function () {
      this.goTestAlarm();
    }.bind(this), 30000);
  }
  _goRecommend() {
    this.props.navigator.push({
      component: Recommend,
      title: 'Recommend',
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: "red",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});

I18n.fallbacks = true;

I18n.translations = {
  en: {
    title: 'Speech Alarm',
    subtitle: 'The more motivating wakeup alarm clock.',
    testAlarm: 'Test Alarm',
    settings: 'Settings',
    recommend: 'Recommend This App',
    titleSettings: 'Speech Alarm - settings',
    alarmTime: "Alarm time",
    repeat: "Choose specific days",
    modules: "Modules",
    moduleOrder: "Module order",
    moduleSettings: "Module settings",
    goBack: "Go back",
    timeToWakeUp: 'Time to wake up!',
    snooze: "Snooze",
    stop: "Stop",
    enterCustomGreeting: "enter custom greeting...",
    standardCustomGreeting: "Wake, rise and shine!",
    test: "Test",
    timeIsNow: "The time is now ",
    andTheDateIs: " and the date is ",
    itIsWeekNumber: "It is week number ",
    failedAccessCal: "Error: failed to access calendar.",
    calNoneNext24: "You have no appointments in the next 24 hours.",
    calUpcoming24: "You have the following appointments within the next 24 hours: ",
    calAtTime: "At ",
    calLocation: "Location: ",
    calTo: " to: ",
    alarmOnOff: "Alarm on/off",
    monday: "Monday",
    tuesday: "Tuesday",
    wednesday: "Wednesday",
    thursday: "Thursday",
    friday: "Friday",
    saturday: "Saturday",
    sunday: "Sunday",
    alarmActive: "The alarm is now speaking. Click to snooze.",
    timeSnooze: "Time for snoozing",
    minutes: "minutes",
    weather: "Weather",
    noWeather: "No weather stations close to you",
    place: "Place",
    time: "Time",
    degCel: "degrees celcius",
    windStrength: "wind strength",
    mps: "meters per second",
    degrees: "degrees"
  },
  nb: {
    title: 'Speech Alarm',
    subtitle: 'Den mer motiverende vekkerklokken.',
    testAlarm: 'Test Alarm',
    settings: 'Innstillinger',
    recommend: 'Anbefal Denne Appen',
    titleSettings: 'Speech Alarm - innstillinger',
    alarmTime: "Tid for alarm",
    repeat: "Velg enkelte dager",
    modules: "Moduler",
    moduleOrder: "Modulenes rekkefølge",
    moduleSettings: "Innstillinger for moduler",
    goBack: "Gå tilbake",
    timeToWakeUp: 'På tide å stå opp!',
    snooze: "Slumre",
    stop: "Stopp",
    enterCustomGreeting: "skriv egendefinert hilsen",
    standardCustomGreeting: "God morgen, stå opp og møt dagen.",
    test: "Test",
    timeIsNow: "Klokken er nå ",
    andTheDateIs: " og datoen er ",
    itIsWeekNumber: "Det er ukenummer ",
    failedAccessCal: "Feil: kunne ikke få tilgang til kalender.",
    calNoneNext24: "Du har ingen avtaler de kommende 24 timene.",
    calUpcoming24: "Du har følgende avtaler innen de neste 24 timene: ",
    calAtTime: "Klokken ",
    calLocation: "Sted: ",
    calTo: " til: ",
    alarmOnOff: "Alarm på/av",
    monday: "Mandag",
    tuesday: "Tirsdag",
    wednesday: "Onsdag",
    thursday: "Torsdag",
    friday: "Fredag",
    saturday: "Lørdag",
    sunday: "Søndag",
    alarmActive: "Alarmen snakker nå. Trykk for å slumre.",
    timeSnooze: "Tid for slumring",
    minutes: "minutter",
    weather: "Været",
    noWeather: "Ingen værstasjoner nær deg",
    place: "Sted",
    time: "tid",
    degCel: "grader celcius",
    windStrength: "vindstyrke",
    mps: "sekundmeter",
    degrees: "grader"
  },
}
