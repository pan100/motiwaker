import React, { Component } from 'react';
import SpeechModuleAbstract from "./SpeechModuleAbstract";
import {
  Text,
  View,
  TextInput
} from 'react-native';

import Button from 'react-native-button';
import I18n from 'react-native-i18n';

import {AsyncStorage} from 'react-native';
import {globals} from "./Globals";

export default class CustomGreetingSentenceMWAModule extends SpeechModuleAbstract {
    constructor(){
        super();
        this._loadInitialState();
        this.state = {
          sentence : "no sentence error"
        }
    }

  _loadInitialState = async () => {
    try {
        
      var value = await AsyncStorage.getItem(globals.storage + ":customGreeting");
      if (value !== null){
        this.state.sentence = value;
        console.log('Recovered selection from disk: ' + value);
      } else {
        this.state.sentence = I18n.t('standardCustomGreeting')
        console.log('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

    _onValueChange = async (sentence) => {
    try {
      await AsyncStorage.setItem(globals.storage + ":customGreeting", sentence);
      this.state.sentence = sentence;
      console.log('Saved selection to disk: ' + this.modules);
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  getSentence() {
    if(typeof this.state.sentence != undefined) {
      return this.state.sentence;
    }
    return "there is no sentence. Error."
  }

    getSettingsComponent() {
        return <View>
            <TextInput 
            placeholder={I18n.t('enterCustomGreeting')}
            defaultValue = {this.getSentence()}
            style={{height: 40}}
            onChangeText={(sentence) => this._onValueChange(sentence)} />
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.speak()}>
                {I18n.t('test')}
            </Button>
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.stop()}>
                {I18n.t('stop')}
            </Button>            
        </View>
    }
}