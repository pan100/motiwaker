package com.motivatingwakeupalarm;

import android.app.Application;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.calendarevents.CalendarEventsPackage;
import com.remobile.localNotifications.RCTLocalNotificationsPackage;
import rni18n.mobile.laplanete.ca.rni18n.RNI18nPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    protected boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new CalendarEventsPackage(),
            new RCTLocalNotificationsPackage(),
            new RNI18nPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
      return mReactNativeHost;
  }
}
