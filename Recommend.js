import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Button from 'react-native-button';
import Speech from 'react-native-speech';
import I18n from 'react-native-i18n';

export default class MotivatingWakeupAlarm extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          MotiWaker
        </Text>
        <Text style={styles.instructions}>
          The more motivating wakeup alarm clock.
        </Text>
       
          <Button
            style={{ fontSize: 20, color: 'green' }}
            styleDisabled={{ color: 'red' }}
            onPress={() => this._goBack()}>
            {I18n.t('goBack')}
          </Button>
        </View>
    );
  }

    _goBack() {
    this.props.navigator.push({
      component: StartPage,
      title: 'Start Page',
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color : "red",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});