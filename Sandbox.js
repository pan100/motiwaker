import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Button from 'react-native-button';
import Speech from 'react-native-speech';

export default class MotivatingWakeupAlarm extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          MotiWaker
        </Text>
        <Text style={styles.instructions}>
          The more motivating wakeup alarm clock.
        </Text>
       
      <Button
        style={{fontSize: 20, color: 'green'}}
        styleDisabled={{color: 'red'}}
        onPress={() => this._handlePress()}>
        Press Me!
      </Button>
      </View>
    );
  }

  _handlePress() {
    var time = new Date().toLocaleTimeString();
    var date = new Date().toLocaleDateString();
      Speech.speak({
  text: 'God morgen. Klokken er nå ' + time + 'og det er ' + date,
  voice: 'nb-NO'
});
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color : "red",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});