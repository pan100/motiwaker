import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    Picker,
    StyleSheet
} from 'react-native';

import AlarmManager from "./AlarmManager";

export default class TimePickerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hour: 7,
            minute: 0,
        }
        this.alarmManager = AlarmManager.instance();
        this.alarmManager.loadInitialState().then(function () {
            this.state.hour = this.alarmManager.settings.hour;
            this.state.minute = this.alarmManager.settings.minute;
            this.forceUpdate();
        }.bind(this));
    }

    setHour(value) {
        this.alarmManager.changeValue("hour", value);
        this.setState({ hour: value })
    }

    setMinute(value) {
        this.alarmManager.changeValue("minute", value);
        this.setState({ minute: value })
    }

    render() {
        let hours = [];
        for (let i = 0; i <= 23; i++) {
            hours.push(<Picker.Item key={"hour" + i} label={i + ""} value={i} />)
        }
        let hourPicker =
            <Picker
                style={styles.picker}
                selectedValue={this.state.hour}
                onValueChange={(hourSelected) => this.setHour(hourSelected)}>
                {hours}
            </Picker>
        let minutes = [];
        for (let i = 0; i <= 59; i++) {
            let label = i;
            if (label < 10) {
                label = "0" + label;
            }
            label = label + "";
            minutes.push(<Picker.Item key={"min" + i} label={label} value={i} />)
        }

        let minutePicker =
            <Picker
                style={styles.picker}
                selectedValue={this.state.minute}
                onValueChange={(minuteSelected) => this.setMinute(minuteSelected)}>
                {minutes}
            </Picker>

        return (
            <View style={styles.flexRow}>
                {hourPicker}
                {minutePicker}
            </View>);

    }
}

var styles = StyleSheet.create({
    flexRow: {
        flexDirection: 'row'
    },
    picker: {
        width: 100,
        flex: 2,
    },
});