import React, { Component } from 'react';
import { AppRegistry, Navigator, Text, View, StyleSheet } from 'react-native';

import StartPage from './StartPage';
import Recommend from './Recommend';
import Alarm from './Alarm';
import Settings from './Settings';

class MotivatingWakeupAlarm extends Component {
  render() {
    const routes = [
      {component: StartPage, index:0} ,{component: Alarm, index:1}, {component: Settings, index:2}, {component: Recommend, index:3}
    ];
    const renderer = function(route, navigator) {
      let RouteComponent = route.component
      return <RouteComponent navigator={navigator} {...route.passProps} />
    }
    return (
      <Navigator
        initialRoute={routes[0]}
        initialRouteStack={routes}
      renderScene={renderer}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});

AppRegistry.registerComponent('MotivatingWakeupAlarm', () => MotivatingWakeupAlarm);
