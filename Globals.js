export const globals = {
    storage : "@motiwaker",
    deviceLocale : require('react-native').NativeModules.RNI18n.deviceLocale,
    timeOffset : new Date().getTimezoneOffset() / 60,
    wundergroundRoot: "http://api.wunderground.com/api/76b5a4b618ee1833/"
}