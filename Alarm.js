import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Button from 'react-native-button';
import Speech from 'react-native-speech';
import I18n from 'react-native-i18n';
import globals from './Globals';
import ModuleRegister from './ModuleRegister';
import StartPage from "./StartPage";

export default class Alarm extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {I18n.t('title')}
        </Text>
        <Text style={styles.instructions}>
          {I18n.t('subtitle')} {I18n.t('timeToWakeUp')}
        </Text>

        <Button
          style={{ fontSize: 20, color: 'green' }}
          styleDisabled={{ color: 'red' }}
          onPress={() => this._snooze()}>
          {I18n.t('snooze')}
        </Button>
        <Button
          style={{ fontSize: 20, color: 'green' }}
          styleDisabled={{ color: 'red' }}
          onPress={() => this._stop()}>
          {I18n.t('stop')}
        </Button>
        <Button
          style={{ fontSize: 20, color: 'green' }}
          styleDisabled={{ color: 'red' }}
          onPress={() => this._goBack()}>
          {I18n.t('goBack')}
        </Button>
      </View>
    );
  }
  _goBack() {
    this.props.navigator.push({
      component: StartPage,
      title: 'Start Page',
    })
  }
  _snooze() {
    Speech.stop();
  }
  _stop() {
    Speech.stop();
  }
  static playAlarm() {
    let moduleRegister = ModuleRegister.instance();
    let modules = moduleRegister.getModules();
    let speechString = "";
    for(module of modules) {
      let mwa = moduleRegister.getMwaModule(module.id);
      if(mwa != null) {
        speechString += mwa.getSentence();
      }
    }

    Speech.speak({
      text: speechString,
      voice: require('react-native').NativeModules.RNI18n.locale
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: "red",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});