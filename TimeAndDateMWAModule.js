import React, { Component } from 'react';
import SpeechModuleAbstract from "./SpeechModuleAbstract";
import {
  Text,
  View,
  TextInput
} from 'react-native';

import Button from 'react-native-button';
import I18n from 'react-native-i18n';
import Speech from 'react-native-speech';

import {AsyncStorage} from 'react-native';
import {globals} from "./Globals";

export default class TimeAndDateMWAModule extends SpeechModuleAbstract {
    constructor(){
        super();
        this.state = {
            sentence : "call getSentence for sentence."
        }
    }

    getSentence() {
    let obj = new Date();
    let time = obj.getHours() + " " + obj.getMinutes();
    time = time.replace(".", " ");
    let date = obj.toLocaleDateString();
    let day = "";
    switch(obj.getDay()) {
      case 0: day = "søndag";
      break;
      case 1: day = "mandag";
      break;
      case 2: day = "tirsdag";
      break;
      case 3: day = "onsdag";
      break;
      case 4: day = "torsdag";
      break;
      case 5: day = "fredag";
      break;
      case 6: day = "lørdag";
      break;
    }
    let onejan = new Date(new Date().getFullYear(), 0, 1);
    let weekNumber = Math.ceil((((new Date() - onejan) / 86400000) + onejan.getDay() + 1) / 7);

    return I18n.t('timeIsNow')
     + time 
     + I18n.t('andTheDateIs') 
     + day + " " 
     + date + "." 
     + I18n.t('itIsWeekNumber') 
     + weekNumber;

//       Speech.speak({
//   text: 'God morgen. Klokken er nå ' + time + 'og det er ' + day + " " + date + ". Det er ukenummer " + weekNumber,
//   voice: 'nb-US'
// });
    }

    speak() {
    Speech.speak({
        text: this.getSentence(),
        voice: I18n.locale
    });
    }

    getSettingsComponent() {
        return <View>
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.speak()}>
                {I18n.t('test')}
            </Button>
            <Button
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this.stop()}>
                {I18n.t('stop')}
            </Button>            
        </View>
    }
}