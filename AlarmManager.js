/*
* Modules:
* Custom greeting sentence
* Time and date
* Calendar briefing
* RSS
* Web radio
* Weather
*/
import { AsyncStorage,
    AppState } from 'react-native';
import { globals } from "./Globals";
import I18n from 'react-native-i18n';

import ModuleRegister from "./ModuleRegister";
import Speech from 'react-native-speech';
import LocalNotification from '@remobile/react-native-local-notifications';

let _singleton = Symbol();

class AlarmManager {

    settings = {};

    constructor(singletonToken) {
        if (_singleton !== singletonToken) {
            throw new Error('Cannot instantiate directly.');
        }
        //load state from storage
        this.loadInitialState().done();
        LocalNotification.on("click", function (notification) {
            this.snoozeAlarm();
        }.bind(this), this);
    }

    static instance() {
        if (!this[_singleton])
            this[_singleton] = new AlarmManager(_singleton);

        return this[_singleton]
    }

    setAlarm() {
        let noRepeatsAndRepeatActive = false;
        if(!this.hasNoRepeats() && this.settings.repeat) {
            noRepeatsAndRepeatActive = true;
        }
        

        if(!noRepeatsAndRepeatActive) {
            let now = new Date();
            let alarmTime = new Date();
            alarmTime.setHours(this.settings.hour);
            alarmTime.setMinutes(this.settings.minute);
            alarmTime.setSeconds(0);
            if(alarmTime > now && !this.settings.repeatDay[new Date().getDay() + 1] && this.settings.repeat) {
                alarmTime.setDate(alarmTime.getDate() + this.daysUntilNextDay());
            }
            if (alarmTime < now) {
                alarmTime.setDate(alarmTime.getDate() + this.daysUntilNextDay());
            }
            let milliseconds = alarmTime.getTime() - now.getTime();
            console.log(milliseconds, "milliseconds");
            this.timeOut = setTimeout(function() {
                this.playAlarm();
            }.bind(this), milliseconds);
            LocalNotification.schedule({
                id: 10,
                title: I18n.t("title"),
                text: I18n.t("alarmActive"),
                at: alarmTime.getTime()-2000,
                sound: null
            });
        }
    }

    playAlarm() {
            let moduleRegister = ModuleRegister.instance();
            moduleRegister.prepareMWAModules();
            setTimeout(function () {
                let modules = moduleRegister.getModules();
                let speechString = "";
                for (module of modules) {
                    let mwa = moduleRegister.getMwaModule(module.id);
                    if (mwa != null) {
                        speechString += mwa.getSentence();
                    }
                }

                Speech.speak({
                    text: speechString,
                    voice: I18n.locale
                });
            }, 2000);
        this.rescheduleNextDay();
    }

    rescheduleNextDay(){
        let noRepeatsAndRepeatActive = false;
        if(!this.hasNoRepeats() && this.settings.repeat) {
            noRepeatsAndRepeatActive = true;
        }

        if(!noRepeatsAndRepeatActive) {
            let now = new Date();
            let alarmTime = new Date();
            alarmTime.setDate(alarmTime.getDate() + this.daysUntilNextDay());
            let milliseconds = alarmTime.getTime() - now.getTime();
            console.log(milliseconds, "milliseconds");
            this.timeOut = setTimeout(function() {
                this.playAlarm();
            }.bind(this), milliseconds);
            LocalNotification.schedule({
                id: 10,
                title: I18n.t("title"),
                text: I18n.t("alarmActive"),
                at: alarmTime.getTime()-2000,
                sound: null
            });     
        }   
    }

    hasNoRepeats() {
        for(let i = 1; i <=7; i++) {
            if(this.settings.repeatDay[i]) {
                return false;
            }
        }
        return true;
    }

    daysUntilNextDay() {
        let today = new Date().getDay() +1;
        let days = 1;
        let lookupDay;
        if(today==7) {
            lookupDay=1;
        }
        else {
            lookupDay = today+1;
        }
        for(let i = lookupDay; i<=7; i++) {
            if(!this.settings.repeatDay[i]) {
                days++;
            }
            else {
                return days;
            }
        }
        for(let i = 1; i<=7; i++) {
            if(!this.settings.repeatDay[i]) {
                days++;
            }
            else {
                return days;
            }
        }        
    }

    unsetAlarm() {
        LocalNotification.cancelAll(function() {
        }, this);
        clearTimeout(this.timeOut);
        this.timeOut = null;
    }

    snoozeAlarm() {
        Speech.stop();

        let now = new Date();
        let alarmTime = new Date();
        alarmTime.setTime(alarmTime.getTime() + (this.settings.snoozeTime*60*1000));
        console.log(this.settings.snoozeTime, "this.snoozeTime");
        console.log(alarmTime,  "alarmTime");
        console.log(alarmTime.getTime(), "alarmTime.getTime()");
        let milliseconds = alarmTime.getTime() - now.getTime();
        this.timeOut = setTimeout(function() {
            this.playAlarm();
        }.bind(this), milliseconds);
        LocalNotification.schedule({
            id: 10,
            title: I18n.t("title"),
            text: I18n.t("alarmActive"),
            at: alarmTime.getTime()-2000,
            sound: null
        });
    }

    changeValue(key, value) {
        console.log(key, "key in changeValue");
        if (key in this.settings) {
            this.settings[key] = value;
            this._onValueChange();
        }
        if(key === "alarmOn") {
            if(value == true) {
                this.setAlarm();
            }
            else {
                this.unsetAlarm();
            }
        }
        if(key === "hour" || key === "minute") {
            if(this.settings.alarmOn) {
                this.unsetAlarm()
                this.setAlarm();
            }
        }
    }

    setRepeatDay(dayNumber) {
        this.settings.repeatDay[dayNumber] = !this.settings.repeatDay[dayNumber];
    }

    loadInitialState = async () => {
        try {
            var value = await AsyncStorage.getItem(globals.storage + ":alarm");
            if (value !== null) {
                this.settings = JSON.parse(value);
                console.log('Recovered selection from disk: ' + value);
            } else {
                this.settings = {
                    alarmOn: false,
                    repeat: false,
                    repeatDay: {
                        1: true,
                        2: true,
                        3: true,
                        4: true,
                        5: true,
                        6: false,
                        7: false
                    },
                    hour: 7,
                    minute: 0,
                    snoozeTime: 10,
                    timeOut: null
                };
                console.log('Initialized with no selection on disk.');
            }
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    _onValueChange = async () => {
        try {
            await AsyncStorage.setItem(globals.storage + ":alarm", JSON.stringify(this.settings));
            console.log('Saved selection to disk: ' + this.settings);
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    deleteSavedData = async () => {
        try {
            await AsyncStorage.removeItem(globals.storage + ":alarm");
            console.log('Selection removed from disk.');
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };
}

export default AlarmManager;